# README #

### How do I get set up? ###

Clone the repository

`git clone git@bitbucket.org:aleckleyer/api_alerts.git`

Install the requirements

`pip install -r requirements.txt`

Minimum required python version: `3.6`

### Running the script ###

Help Screen
```bash
λ python api_alerts.py --help
Usage: api_alerts.py [OPTIONS] CURRENCY

  Create instance of currency and track alerts

Options:
  -d, --deviation TEXT  [required]
  --help                Show this message and exit.
```

Checking for a single deviation
```bash
λ python api_alerts.py ethbtc -d 1
{ "timestamp": 2021-12-16 10:47:34,188,
  "level": INFO,
  "trading_pair": ethbtc,
  "deviation": True,
  "data": { 'last_price': 0.08411,
            'average': 0.08190791666666668,
            'change': 0.0022020833333333267,
            'sdev': 1.617590358964841}}
```

### Code Tests ###
PyLint Score

`Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)`

### Future Potential ###

##### Improvements #####
In the future, this script can be improved by threading calls to APIs to work in tandem.

Utilizing the `concurrent.futures` library to thread off API calls will speed up
the script execution as new features are added.

Additionally, utilizing the threading feature can allow the script to take in multiple
currency pairs as input. We can thread off each currency pair as a thread to reduce
number of calls to this script.

##### Additions #####
Tracking the deviation of the last price from the 24hr avg is useful to determine
the volatility of the currency pair price, but there are a lot more useful metrics
and indicators that we can add in the future.

**RSI**

With the Relative Strength Index we can determine if the currency pair is being
overbought or oversold and alert on those two outcomes

**MACD**

The Moving Average Convergance Divergance indicator can help determine the beginning
or end, of a bull or bear market for a security. We can trade on the fast signal line
crossing over the slow signal line, which potentially indicates a reverse in the
stock movement.

### Extra ###
**Total Time Taken**

~ 3 hours for coding; ~ 30 mins for the ReadMe

**Difficulties**

Taking time to accomplish this around work hours was difficult, but I managed to
piece together a few hours during this week to accomplish it.

Additionally, I needed to do a little research on best logging practices after testing
my code against PyLint. I learned a lot about how to properly use logging as a singleton.

If I had more time I would have implemented more mathematical error catching. Right now
there is only a validation of symbols, and not for mathematical errors.
