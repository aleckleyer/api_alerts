"""This script takes a currency pair as input
and deviation value to determine if the last price
of the passed currency pair is that many deviations
away from the mean.
"""

import sys
import logging
import statistics
import requests
import click


def avg(in_list):
    """ Return avg of a list
    """
    return sum(in_list) / len(in_list)


class GeminiCurrency():
    """ Object for interacting with a currency trading pair
    """

    def __init__(self, currency_pair, logger):
        """ Set trading pair and validate it exists
        """
        self.currency_pair = currency_pair
        self.base_url = "https://api.gemini.com/"
        self.logger = logger.logger
        self.extra = {
            'trading_pair': self.currency_pair,
            'deviation_found': "N/A"
        }
        self._validate_symbol()

    def _validate_symbol(self):
        """ Function to search for symbol in gemini db
        """
        res = self._api_get("/symbols").json()
        if self.currency_pair not in res:
            self.logger.error(
                f"{self.currency_pair} is invalid.", extra=self.extra)
            sys.exit(1)

    def _api_get(self, ext):
        """ Set v1 api
        """
        return requests.get(self.base_url + 'v1' + ext)

    def _api_get_v2(self, ext):
        """ Set v2 api
        """
        return requests.get(self.base_url + 'v2' + ext)

    def get_day_prices(self):
        """ Return prices from past 24 hr
        """
        return self._api_get_v2(
            "/ticker/" + self.currency_pair).json()['changes']

    def get_current_price(self):
        """ Return current price
        """
        res = self._api_get("/pricefeed").json()
        for key in res:
            if key['pair'] == self.currency_pair.upper():
                return float(key['price'])
        return "N/A"

    def run_alert(self, deviation_filter):
        """ Run stdev test and print alert
        """
        current_price = self.get_current_price()
        day_price_list = list(map(float, self.get_day_prices()))
        day_avg = avg(day_price_list)
        std_dev = abs(statistics.stdev(day_price_list))
        current_std_dev = abs((current_price - day_avg)) / std_dev
        if current_std_dev >= float(deviation_filter):
            self.extra['deviation_found'] = True
        else:
            self.extra['deviation_found'] = False
        data = {
            'last_price': current_price,
            'average': day_avg,
            'change': abs(current_price - day_avg),
            'sdev': current_std_dev}

        self.logger.info(data, extra=self.extra)


class Logger():
    """ Define basic logger
    """

    def __init__(self):
        """ Setup logger
        """
        self.logger = logging.getLogger('api_alerts')
        self.fmt = """
        {"timestamp": %(asctime)s,
          "level": %(levelname)s,
          "trading_pair": %(trading_pair)s,
          "deviation": %(deviation_found)s,
          "data": %(message)s}""".strip()
        logging.basicConfig(format=self.fmt)
        self.logger.setLevel("DEBUG")

    def get_fmt(self):
        """ Return fmt for logger
        """
        return self.fmt

    def set_fmt(self, fmt):
        """ Override fmt for logger
        """
        self.fmt = fmt
        logging.basicConfig(format=self.fmt)


@click.argument('currency')
@click.option('--deviation', '-d', required=True)
def _run(currency, deviation):
    """ Create instance of currency and track alerts
    """
    logger = Logger()
    currency_pair = GeminiCurrency(currency, logger)
    currency_pair.run_alert(deviation)


run = click.command()(_run)

if __name__ == '__main__':
    run()
